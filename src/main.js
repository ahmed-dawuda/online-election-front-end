/* eslint-disable no-undef */
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import VeeValidate from 'vee-validate'
import Auth from './packages/authentication/auth'
import Loader from './packages/loader/loader'
import _ from 'lodash'
import Vuex from 'vuex'

// router.beforeEach((to, from, next) => {
//   // console.log(from)
//   if (from.name === 'ballot' && to.name !== 'voter-login') {
//     swal({
//       title: 'Are you sure?',
//       text: 'Your vote has not been saved yet, you will lose everything',
//       icon: 'warning',
//       buttons: true,
//       dangerMode: true
//     })
//       .then((willCast) => {
//         if (willCast) {
//           next()
//         } else {
//           next(from)
//         }
//       })
//   } else {
//     next()
//   }
// })

Vue.config.productionTip = false
Vue.use(Auth)
Vue.use(VeeValidate)
Vue.use(Loader)
Vue.use(VueAxios, Axios)
Vue.use(_)
Vue.use(Vuex)

Axios.defaults.baseURL = 'http://election.local/'

Axios.interceptors.request.use((config) => {
  let apiToken = Vue.auth.getToken()
  if (apiToken && !config.headers.common.Authorization) {
    config.headers.common.Authorization = `Bearer ${apiToken}`
  }
  return config
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
