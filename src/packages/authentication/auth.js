export default (Vue) => {
  Vue.auth = {
    getToken () {
      const token = localStorage.getItem('token')
      const expiration = localStorage.getItem('expiration')

      if (!token || !expiration) {
        return null
      }
      return token
    },
    setToken (token, expiration) {
      localStorage.setItem('token', token)
      localStorage.setItem('expiration', expiration)
    },
    destoryToken () {
      localStorage.removeItem('token')
      localStorage.removeItem('expiration')
    },
    isAuthenticated () {
      if (this.getToken()) {
        return true
      } else {
        return false
      }
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}
