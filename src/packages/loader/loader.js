/* eslint-disable no-undef */
export default (Vue) => {
  Vue.loader = {
    show () {
      $('#loader').modal({backdrop: 'static'})
    },
    hide () {
      $('#loader').modal('hide')
    }
  }
  Object.defineProperties(Vue.prototype, {
    $loader: {
      get () {
        return Vue.loader
      }
    }
  })
}
