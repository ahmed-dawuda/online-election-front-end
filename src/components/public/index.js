import login from './login.vue'
import parent from './parent.vue'
import dashboard from './voterDashboard.vue'
import main from './main.vue'
import elections from './elections.vue'
import verify from './verify.vue'
import ballot from './ballot.vue'
import results from './results.vue'

export const PublicLogin = login
export const PublicParent = parent
export const VoterDashboard = dashboard
export const PublicMain = main
export const PublicElections = elections
export const Verify = verify
export const Ballot = ballot
export const Results = results
