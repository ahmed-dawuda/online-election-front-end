import login from './login.vue'
import parent from './parent.vue'
import register from './register.vue'
import dashboard from './dashboard.vue'
import adminHome from './home.vue'
import voters from './voters.vue'
import categories from './categories.vue'
import elections from './elections.vue'
import messages from './inbox.vue'
import allvoters from './allVoters.vue'
import adminElectionDetail from './electionPage.vue'
import manageCandidate from './manageCandidate.vue'
import summary from './allsummary.vue'

export const Login = login
export const Parent = parent
export const Register = register
export const Dashboard = dashboard
export const AdminHome = adminHome
export const Voters = voters
export const Categories = categories
export const Elections = elections
export const Messages = messages
export const AllVoters = allvoters
export const AdminElectionDetail = adminElectionDetail
export const ManageCandidate = manageCandidate
export const AllSummary = summary
