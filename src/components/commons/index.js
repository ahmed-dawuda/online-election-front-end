import nonAuthHeader from './non-auth/header.vue'
import adminHeader from './auth/admin-header.vue'
import publicHeader from './auth/public-header.vue'

export const NonAuthHeader = nonAuthHeader
export const PublicHeader = publicHeader
export const AdminHeader = adminHeader
