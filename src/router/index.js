import Vue from 'vue'
import Router from 'vue-router'
import {
  Login,
  Parent,
  Register,
  Dashboard,
  AdminHome,
  Voters,
  Categories,
  Elections,
  Messages,
  AllVoters,
  AdminElectionDetail,
  ManageCandidate,
  AllSummary
} from './../components/admin'
import {
  PublicLogin,
  PublicParent,
  PublicMain,
  PublicElections,
  Verify,
  Ballot,
  Results} from '../components/public'

import Welcome from './../components/welcome.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      name: 'welcome',
      path: '/',
      component: Welcome
    },
    {
      name: 'voter',
      path: '/voter',
      component: PublicParent,
      children: [
        {
          name: 'voter-login',
          path: 'voter/login',
          component: PublicLogin
        },
        {
          name: 'main',
          path: 'public',
          component: PublicMain,
          children: [
            {
              name: 'voter-dashboard',
              path: 'dashboard',
              component: PublicElections
            },
            {
              name: 'ballot',
              path: 'ballot',
              component: Ballot
            },
            {
              name: 'results',
              path: 'results',
              component: Results
            }
          ]
        },
        {
          name: 'verify',
          path: 'verify',
          component: Verify
        }
      ]
    },
    {
      name: 'ec',
      path: '/ec',
      component: Parent,
      children: [
        {
          name: 'ec-login',
          path: 'login',
          component: Login
        },
        {
          name: 'ec-register',
          path: 'register',
          component: Register
        },
        {
          name: 'dashboard',
          path: 'admin',
          component: Dashboard,
          children: [
            {
              name: 'admin-dashboard',
              path: 'dashboard',
              component: AdminHome
            },
            {
              name: 'admin-voters',
              path: 'voters',
              component: Voters
            },
            {
              name: 'admin-categories',
              path: 'categories',
              component: Categories
            },
            {
              name: 'admin-elections',
              path: 'elections',
              component: Elections
            },
            {
              name: 'admin-messages',
              path: 'messages',
              component: Messages
            },
            {
              name: 'admin-all-voters',
              path: 'all-voters',
              component: AllVoters
            },
            {
              name: 'admin-election-detail',
              path: 'admin-election-detail',
              component: AdminElectionDetail
            },
            {
              name: 'manage-candidates',
              path: 'manage-candidates',
              component: ManageCandidate
            },
            {
              name: 'summary',
              path: 'summary',
              component: AllSummary
            }
          ]
        }
      ]
    }
  ]
})
